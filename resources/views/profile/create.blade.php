@extends('layout.master')
@section('title')
Halaman Profile
@endsection
@section('content')

<form method="POST" action="/profile">
    @csrf
    <div class="form-group">
      <label>Nama Lengkap</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror

    <div class="form-group">
      <label>umur</label>
      <input type="text" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
   
<div class="form-group">
    <label>TTL</label>
    <input type="text" name="bio" class="form-control">
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror

  <div class="form-group">
    <label>alamat</label>
    <input type="text" name="alamat" class="form-control">
  </div>
  
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection