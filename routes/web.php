<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', 'DashbordController@index');
Route::get('/shop', 'DashbordController@shop');
// Route::get('/biodata', 'FormController@bio');
// route::post('/kirim','FormController@kirim');

// Route::get('/data-table', 'DashbordController@table');

// Route::group(['middleware' => ['auth']], function () {


// //crud kategori
// //create
// route::get('/kategori/create', 'KategoriController@create'); //mengarah ke form tambah database
// Route::post('/kategori','KategoriController@store'); //menyimpan data form ke database table katagori

// //read
// Route::get('/kategori', 'KategoriController@index'); //ambil data ke data di tampilan di blade
// Route::get('/kategori/{kategori_id}', 'KategoriController@show'); //route detail kategori

// //upload
// Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit'); //route untuk mengarah ke form edit
// Route::put('/kategori/{kategori_id}', 'KategoriController@update'); //route untuk mengarah ke form update

// //delete
// Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy'); // route delete data berdasarkan id
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');



//CRUD profile
Route::get('/profile/create', 'ProfileController@create');
Route::post('/profile', 'ProfileController@submit');