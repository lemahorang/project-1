<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio()
        {
            return view('page.daftar');
        }
    
        public function kirim(Request $request )
        {
                
            
            $nama_depan  = $request['first_name'];
            $nama_belakang  = $request['last_name'];

            return view('page.hallo', compact("nama_depan","nama_belakang"));
        }
}
