<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Dashbordcontroller extends Controller
{
    public function index()
    {
        return view('page.index');
    }

    public function shop()
    {
        return view('page.shop');
    }
}
