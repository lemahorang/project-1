<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = 'profil';
    protected $fillable = ["nama", "umur", "bio","alamat","user_id"];
}
